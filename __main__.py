import random, getpass, itertools, json

COLORS = [
    {'code': 'R', 'label': 'Rouge'},
    {'code': 'V', 'label': 'Vert'},
    {'code': 'B', 'label': 'Bleu'},
    {'code': 'J', 'label': 'Jaune'},
    {'code': 'O', 'label': 'Orange'},
    {'code': 'N', 'label': 'Noir'},
    {'code': 'M', 'label': 'Marron'},
    {'code': 'G', 'label': 'Gris'}
]
NB_HOLES = 4
MAX_TRIES = 12

def proposeHelp():
    print('-- configuration du jeu --')
    print(f'  Nombre de couleurs dans une combination : {NB_HOLES}')
    print(f'  Nombre de tentatives : {MAX_TRIES}')
    print('  Couleurs disponnibles : (couleur [code])')
    print('  ' + ', '.join(list(map(lambda x: f'{x["label"]} [{x["code"]}]', COLORS))))
    print('--------------------------')
    print('')
    answer = input('Souhaitez-vous un rappel des règles ? (y/n) ')
    if answer.lower() not in ['y', 'n']:
        print('La réponse ne correspond pas à "y" ou "n"')
        exit(1)
    elif answer.lower() == 'n':
        return
    elif answer.lower() == 'y':
        print('-- règles --')
        print('  Dans mastermind, deux joueurs s\'affrontent')
        print(f'  le premier choisit une combination de {NB_HOLES} couleurs, parmis : {", ".join(list(map(lambda x: x["label"], COLORS)))}')
        print(f'  le second joueur s\'efforce de trouver la bonne combination, en faisant plusieurs tentatives successives, jusqu\'à un maximum de {MAX_TRIES}')
        print('  après chaque tentative, le premier joueur (ou le programme, ici) l\'informe des couleurs qui sont présentes au bon endroit, et de celles présentes dans la combination mais au mauvais endroit')
        print('  le joueur peut alors se baser sur ces informations pour ses tentatives suivantes, jusqu\'à trouver la bonne combination ou jusq\'à épuiser le nombre de tentatives disponibles')
        print('')
        print('  Mode 0 joueurs : deux IA s\'affrontent, aucun controle n\'est requis')
        print('  Mode 1 joueur  : une IA définit une combination secrète, le joueur doit la trouver')
        print('  Mode 2 joueurs : le premier joueur définit une combination secrète, le second doit la trouver')
        print('')
        print('  Déroulement d\'une partie : ')
        print('  1. Définition de la combination secrète (mode 2 joueurs seulement)')
        print('  2. Sélection d\'une combination de couleur. Cette étape se répète jusqu\'à trouver la bonne combination ou jusqu\'à épuiser le nombre de tentatives disponibles')
        print('  3. Affichage des résultats')
        print('------------')
        print('')

def chooseNbPlayers() :
    nb_players = int(input('Choisissez le nombre de joueurs : (0/1/2) '))
    if nb_players not in [0, 1, 2]:
        print('La réponse n\'est pas comprise parmis 0, 1, 2')
        exit(1)
    return nb_players

def chooseCombination(nb_players):
    print('-- choix de la combination secrète --')
    combination = []
    if nb_players in [0, 1]:
        combination = [(x, COLORS[random.randrange(0, len(COLORS)-1, 1)]['code']) for x in range(0, NB_HOLES)]
        print(f'  combination définie (nombre de joueurs : {nb_players})')
        print('-------------------------------------')
        print('')
        return combination
    
    print(f'  Saisir le code de la couleur parmis [{", ".join(list(map(lambda x: x["code"], COLORS)))}] (la valeur ne sera pas affichée)')
    for i in range(0, NB_HOLES):
        combination.append((i, getpass.getpass('  Choisissez une couleur : ').upper()))
    for color in combination:
        if color[1] not in list(map(lambda x: x["code"], COLORS)):
            print(f'  La couleur "{color[1]}" n\'est pas valide')
            exit(1)
    print(f'  combination définie (nombre de joueurs : {nb_players})')
    print('-------------------------------------')
    print('')
    return combination

def promptForCombination():
    combination = []
    globalError = True
    while globalError:
        error = False
        print(f'Saisir le code de la couleur parmis [{", ".join(list(map(lambda x: x["code"], COLORS)))}]')
        for i in range(0, NB_HOLES):
            combination.append((i, input('  Choisissez une couleur : ').upper()))
        for color in combination:
            if color[1] not in list(map(lambda x: x["code"], COLORS)):
                print(f'  La couleur "{color[1]}" n\'est pas valide')
                combination = []
                error = True
        if not error:
            globalError = False
    return combination

def compareCombinations(combination, player_combination):
    colors_in_right_pos = []
    colors_in_wrong_pos = []
    for color in player_combination:
        if color in combination:
            colors_in_right_pos.append(color)

    for color in player_combination:
        combination_colors = list(map(lambda x: x[1], combination))
        if color not in combination and color[1] in combination_colors:
            right_pos_wrong_pos_colors = list(map(lambda x: x[1], colors_in_wrong_pos)) + list(map(lambda x: x[1], colors_in_right_pos))
            if len(list(filter(lambda x: x == color[1], right_pos_wrong_pos_colors))) + 1 <= len(list(filter(lambda x: x == color[1], combination_colors))):
                colors_in_wrong_pos.append(color)

    return {'colors_in_right_pos': colors_in_right_pos, 'colors_in_wrong_pos': colors_in_wrong_pos}
    
            
def playGame(nb_players, combination):
    tries = 0
    if nb_players in [1, 2]:
        for i in range(0, MAX_TRIES):
            player_combination = promptForCombination()
            result = compareCombinations(combination, player_combination)
            tries += 1
            if len(result['colors_in_right_pos']) == NB_HOLES:
                return {'result': 'win', 'player_combination': player_combination}
            else:
                print(f'Nombre de couleurs à la bonne position : {len(result["colors_in_right_pos"])}')
                print(f'Nombre de couleurs à la mauvaise position : {len(result["colors_in_wrong_pos"])}')
                print(f'Nombre de couleurs absentes de la combinaision : {NB_HOLES - len(result["colors_in_right_pos"]) - len(result["colors_in_wrong_pos"])}')
                print(f'Nombre de tentatives restantes : {MAX_TRIES - tries}')
                print('---')
        return {'result': 'lost', 'player_combination': player_combination}
    else:
        # implémenter l'IA
        print('Déchiffrage de la combinaison : ')
        all_possible_guess = []
        positions_per_colors = []
        for color in list(map(lambda x: x["code"], COLORS)):
            for i in range (0, NB_HOLES):
                positions_per_colors.append((i, color))
        all_possible_guess_inter = list(itertools.combinations(positions_per_colors, 4))
        all_possible_guess = all_possible_guess_inter
        to_remove = []
        for g in all_possible_guess_inter:
            indexes = []
            for c in g:
                if c[0] in indexes:
                    to_remove.append(g)
                    continue
                indexes.append(c[0])
        for g in to_remove:
            if g in all_possible_guess:
                all_possible_guess.remove(g)

        guess = all_possible_guess[random.randrange(0, len(all_possible_guess)-1, 1)]
        result = compareCombinations(combination, guess)
        all_possible_guess.remove(guess)

        print(guess)
        print(f'Nombre de couleurs à la bonne position : {len(result["colors_in_right_pos"])}')
        print(f'Nombre de couleurs à la mauvaise position : {len(result["colors_in_wrong_pos"])}')
        print(f'Nombre de couleurs absentes de la combinaision : {NB_HOLES - len(result["colors_in_right_pos"]) - len(result["colors_in_wrong_pos"])}')
        print(f'Nombre de tentatives restantes : {MAX_TRIES - tries}')
        if len(result['colors_in_right_pos']) == NB_HOLES:
            return {'result': 'win', 'player_combination': guess}
        else:
            for i in range(0, MAX_TRIES-1):
                prev_guess = guess
                prev_result = result
                all_guess_matching_previous_try = []
                to_remove = []

                if len(prev_result["colors_in_right_pos"]) == 0 and len(prev_result["colors_in_wrong_pos"]) == 0:
                    for g in all_possible_guess:
                        for c in list(map(lambda x: x["code"], COLORS)):
                            if c in list(map(lambda x: x[1], prev_guess)):
                                to_remove.append(g)
                
                elif len(prev_result["colors_in_right_pos"]) + len(prev_result["colors_in_wrong_pos"]) == NB_HOLES:
                    for g in all_possible_guess:
                        for c in list(map(lambda x: x[1], prev_guess)):
                            if c not in list(map(lambda x: x[1], g)):
                                to_remove.append(g)
                
                for g in to_remove:
                    if g in all_possible_guess:
                        all_possible_guess.remove(g)

                for g in all_possible_guess:
                    r = compareCombinations(combination, g)
                    if len(r["colors_in_right_pos"]) == len(prev_result["colors_in_right_pos"]) and len(r["colors_in_right_pos"]) >= len(prev_result["colors_in_wrong_pos"]):
                        all_guess_matching_previous_try.append(g)
                    elif len(r["colors_in_right_pos"]) > len(prev_result["colors_in_right_pos"]) and len(r["colors_in_wrong_pos"]) >= NB_HOLES - len(r["colors_in_right_pos"]):
                        all_guess_matching_previous_try.append(g)
                all_possible_guess = all_guess_matching_previous_try
                guess = all_possible_guess[random.randrange(0, len(all_possible_guess)-1, 1)]
                result = compareCombinations(combination, guess)
                tries += 1
                all_possible_guess.remove(guess)
                
                print(guess)
                print(f'Nombre de couleurs à la bonne position : {len(result["colors_in_right_pos"])}')
                print(f'Nombre de couleurs à la mauvaise position : {len(result["colors_in_wrong_pos"])}')
                print(f'Nombre de couleurs absentes de la combinaision : {NB_HOLES - len(result["colors_in_right_pos"]) - len(result["colors_in_wrong_pos"])}')
                print(f'Nombre de tentatives restantes : {MAX_TRIES - tries}')
                if len(result['colors_in_right_pos']) == NB_HOLES:
                    return {'result': 'win', 'player_combination': guess}

        return {'result': 'lost', 'player_combination': guess}

def main():
    # --- initial setup
    combination = []
    tries = []
    nb_players = 0

    # --- play
    print('Bienvenue sur Mastermind !')
    print('')
    proposeHelp()

    nb_players = chooseNbPlayers()
    combination = chooseCombination(nb_players = nb_players)
    print(combination)
    result = playGame(nb_players, combination)

    if result['result'] == 'win':
        print('Victoire !')
    elif result['result'] == 'lost':
        print('Défaite !')

if __name__ == '__main__':
    main()