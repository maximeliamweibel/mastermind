# MASTERMIND
Projet python, pour jouer au mastermind dans le plus grand plaisir

## Comment jouer :
* Cloner le projet
* `python mastermind`

## Algo pour l'IA :
1. On commence par établir toutes les combinaisons possibles
2. On en prend une au hasard, et on la teste
3. Le test nous donne le nombre de couleurs bien positionnées, et le nombre de couleurs mal positionnées. On peut également en déduire le nombre de couleur qui ne sont pas dans la combinaison secrète
4. Si aucune des couleurs testées n'est dans la combinaison secrète, on retire toutes les combinaisons contenant ces couleurs, et on retente avec une combinaison au hasard parmis ce qu'il reste
5. Si la somme des couleurs en bonne position et des couleurs en mauvaise position est égale au nombre de trous dans la combinaisons secrète, on enlève des combinaisons possibles toutes les combinaisons dont une des couleurs n'est pas dans la tentative que l'on vient de faire
5. Sinon, on enlève des combinaisons possibles tout ce qui ne correspond pas au moins au résultat de l'itération précédante, et on recommence